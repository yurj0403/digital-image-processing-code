#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

double point2Line(Point2f pointP, Point2f pointA, Point2f pointB)
{
    //求直线方程
    double A = 0, B = 0, C = 0;
    A = pointA.y - pointB.y;
    B = pointB.x - pointA.x;
    C = pointA.x * pointB.y - pointA.y * pointB.x;
    //代入点到直线距离公式
    double distance = 0;
    double tmp1 = abs(A * pointP.x + B * pointP.y + C);
    double tmp2 = ((float)sqrtf(A * A + B * B));
    distance = tmp1 / tmp2;
    return distance;
}
int main() {
    vector<string> filenames;
    glob("./", filenames);
    // 测试
    for (int n = 0; n < filenames.size(); n++) {
        string filename = filenames[n];
        Mat src = imread(filename, 0);
        int h = getOptimalDFTSize(src.rows);
        int w = getOptimalDFTSize(src.cols);

        Mat padded;
        copyMakeBorder(src, padded, 0, h - src.rows, 0, w - src.cols, BORDER_CONSTANT, Scalar::all(0));

        Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
        Mat complexImg;
        merge(planes, 2, complexImg);
        dft(complexImg, complexImg, DFT_COMPLEX_INPUT | DFT_COMPLEX_OUTPUT);

        split(complexImg, planes);
        Mat magMat;
        magnitude(planes[0], planes[1], magMat);
        magMat += 1;
        log(magMat, magMat);
        normalize(magMat, magMat, 0, 255, NORM_MINMAX);
        magMat.convertTo(magMat, CV_8UC1);
        //4. 把零频移到中心
        Mat magImg = magMat.clone();
        int cx = magImg.cols / 2;
        int cy = magImg.rows / 2;
        Mat q1 = magImg({ 0, 0, cx, cy });
        Mat q2 = magImg({ 0, cy, cx, cy });
        Mat q3 = magImg({ cx, 0, cx, cy });
        Mat q4 = magImg({ cx, cy, cx, cy });
        Mat temp;
        q1.copyTo(temp);
        q4.copyTo(q1);
        temp.copyTo(q4);
        q2.copyTo(temp);
        q3.copyTo(q2);
        temp.copyTo(q3);

        Mat binImg;
        threshold(magImg, binImg, 150, 255, THRESH_BINARY);
        Mat markImg;
        cvtColor(binImg, markImg, COLOR_GRAY2BGR);
        vector<Vec4i> lines;
        Vec4i l;
        HoughLinesP(binImg, lines, 1, CV_PI / 180.0, 30, 200, 50);
        Point2f p = Point2f(magImg.cols / 2.0, magImg.rows / 2.0);
        for (int i = 0; i < lines.size(); i++)
        {
            if (abs(lines[i][1] - lines[i][3]) > 15) {
                line(markImg, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0, 255, 0), 1, 8, 0);


                l = lines[i];
            }
        }
        float theta = atan((l[1] - l[3]) * 1.0 / (l[0] - l[2]) * src.cols / src.rows) * 180 / CV_PI;

        float angle = theta <= 0 ? theta + 90 : theta - 90;
        Point2f center = Point2f(src.cols / 2.0, src.rows / 2.0);
        Mat rotMat = getRotationMatrix2D(center, angle, 1.0);
        Mat dst = Mat::ones(src.size(), CV_8UC1);
        warpAffine(src, dst, rotMat, src.size(), 1, 0, Scalar(255, 255, 255));
        imshow("src", src);
        imshow("markImg", markImg);
        imshow("dst", dst);
        waitKey(5100);
    }
    return 0;
}