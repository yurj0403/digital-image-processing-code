#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

int main() {
    clock_t start = clock();
    Mat src = imread("./2.TIF", 0);
    imshow("src", src);
    // 填充到傅里叶变换最佳尺寸 （gpu下加速明显）
    int h = getOptimalDFTSize(src.rows);
    int w = getOptimalDFTSize(src.cols);
    Mat padded;
    copyMakeBorder(src, padded, 0, h - src.rows, 0, w - src.cols, BORDER_REFLECT, Scalar::all(0));
    Mat pMat = Mat(padded.size(), CV_32FC1);

    // 中心化 （在时域做中心化，傅里叶变换后的频谱图低频在中心）
    for (int i = 0; i < padded.rows; i++)
    {
        for (int j = 0; j < padded.cols; j++)
        {
            pMat.at<float>(i, j) = pow(-1, i + j) * padded.at<uchar>(i, j);
        }
    }

    // 傅里叶变换
    Mat planes[] = { pMat, Mat::zeros(padded.size(), CV_32F) };
    Mat complexImg;
    merge(planes, 2, complexImg);
    dft(complexImg, complexImg, DFT_COMPLEX_OUTPUT);

    // 显示频谱图
    split(complexImg, planes);
    Mat magMat;
    magnitude(planes[0], planes[1], magMat);
    magMat += 1;
    log(magMat, magMat);
    normalize(magMat, magMat, 0, 255, NORM_MINMAX);
    magMat.convertTo(magMat, CV_8UC1);

    // 低通滤波
    Mat mask(Size(w, h), CV_32FC2, Scalar(0, 0));
    circle(mask, Point(w / 2, h / 2), 50, Scalar(255, 255), -1);
    complexImg = complexImg.mul(mask);

    //高通滤波
    //Mat mask(Size(w, h), CV_32FC2, Scalar(255, 255));
    //circle(mask, Point(w / 2, h / 2), 50, Scalar(0, 0), -1);
    //complexImg = complexImg.mul(mask);

    // 傅里叶反变换
    Mat dst, tmp;
    idft(complexImg, tmp, DFT_REAL_OUTPUT);

    // 去中心化
    for (int i = 0; i < padded.rows; i++)
    {
        for (int j = 0; j < padded.cols; j++)
        {
            pMat.at<float>(i, j) = pow(-1, i + j) * tmp.at<float>(i, j);
        }
    }
    dst = pMat({ 0,0,src.cols ,src.rows }).clone();
    normalize(dst, dst, 0, 1, NORM_MINMAX);
    dst.convertTo(dst, CV_8U, 255);
    imshow("dst", dst);
    clock_t end = clock();
    cout << "spend time:" << end - start << "ms" << endl;
    waitKey(0);
    return 0;
}